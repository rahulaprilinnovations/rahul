const mongoose=require('mongoose');
const express=require('express');
const app=express('localhost');
const multer=require('multer');
//var path = require('path');
const port = process.env.PORT || 8000;
const bodyParser =require('body-parser');
const path=require('path');
//const session=require('session');
const cookieParser=require('cookie-Parser');
const session=require('express-session');
const passport=require('passport');


mongoose.Promise = global.Promise;
mongoose.connect(
  'mongodb://localhost:27017/user',


  {
    useNewUrlParser: true
  }
);

const db = mongoose.connection;

db.on(
  'error',
  console.error.bind(
    console, 'MongoDB connection error:'
  )
);
/*---------------------------------------CORS setup---------------------------------------------*/

app.use(function(req, res, next)  {
  var allowedOrigins = ['http://localhost:3000', 'http://localhost:3002'];
  var origin = req.headers.origin;
  if(allowedOrigins.indexOf(origin) > -1){
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Credentials', true);
  return next();
 });
 
 

 db.on('error', console.error.bind(console, 'MongoDB connection error:'));
 app.use(cookieParser());
//app.use(session);
 app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
 app.use(session({ secret: 'soManySecrets', resave: false, saveUninitialized: false }));
 app.use(passport.initialize());
 app.use(passport.session());
 //app.use(router);
 app.use(express.static('./public'));
 app.use(function (request, response, next) {
   next();
 });

var passportConfig=require('../RD/api/config/passport');
passportConfig(passport);

var userRoute=require('../RD/api/routes/userRoute');
 userRoute(app, passport);

app.listen(port);
console.log('node RESTful API server started on: ' + port);



