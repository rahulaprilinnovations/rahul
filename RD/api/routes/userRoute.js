'use strict';

module.exports=function(app,passport)
 {  
   var userController=require('../controllers/userController');
    
     app.route('/user/login')
     .post(passport.authenticate('local-login', {
      successRedirect: '/user/homePage', session: true ,
      failureRedirect: '/user/failure'
  
     }
   )
 );
 
  app.route('/user/homePage')  
 .get(isLoggedIn,userController.homePage);

 app.route('/user/register')  
 .post(userController.register);

app.route('/user/failure')
    .get(userController.failure);

  }

  function isLoggedIn(request, response, next) {
  /*if user is authenticated in the session, carry on*/
  console.log('isLoggedIn function');
  if (request.isAuthenticated()) 
  {
     return next();
  }
  else
  {
    /*if they aren't redirect them to the home page*/
    response.statusCode = 400;
    return response.send(
      {
        status: 'FAILURE',
        message: 'User not logged in'
      }
    );
  }
}


