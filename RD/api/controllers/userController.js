'use strict';
var User = require('../models/userModel').User;

exports.homePage = function(request, response) {
  //console.log('request',request.user)
  var userDetails = [];
  User.findById(request.user._id)
  .exec(function(err, userData)
    {
      if(err)
      {
        console.log(err)
        //response
        response.statusCode = '500';
        return response.send(
           { 
            status: 'Failure', 
            message: 'Error occure .',
            data: userDetails  
           }
        );
      }
      else if(userData == null)
      {
        //response 404 user not found
        response.statusCode = 404;
        return response.send(
        { 
            status: 'not found', 
            message: 'something went wrong.',
            data: userDetails  
        }
      );
      }
      else
      {
        var temp = {}
        temp.userId = userData._id;
        temp.emailId = userData.emailId;
        temp.userName = userData.userName;
        temp.DOB = userData.DOB;
        temp.Gender = userData.Gender;
        temp.maritalStatus = userData.maritalStatus;
        userDetails.push(temp)
        response.statusCode = 200;
        return response.send(
        { 
            status: 'success', 
            message: 'successfull login.',
            data: userDetails  
        }
      );
      }
    }
  )
}

  exports.logout = function(request, response) {
    request.logout();
    response.statusCode = 200;
    return response.send(
      {
        status: 'SUCCESS',
        message: 'Admin logged out successfully.'
      }
    );
  };
  
  exports.failure = function(request, response) {
    response.statusCode = 400;
    return response.send(
    { 
       status: 'FALIURE',
       message: 'Email-Id or password is incorrect.'
    }
   );
  }
  
exports.register=function(request,response)
  {
    
    if(typeof request.query.emailId == 'undefined' || request.query.emailId == null
    || typeof request.query.userName == 'undefined' || request.query.password ==null
    || typeof request.query.password == 'undefined' || request.query.password == null
    || typeof request.query.DOB == 'undefined' || request.query.DOB ==null 
    || typeof request.query.mobileNo == 'undefined' || request.query.mobileNo == null
    || typeof request.query.maritalStatus == 'undefined' || request.query.maritalStatus == null
    || typeof request.query.city == 'undefined' || request.query.city == null
    || typeof request.query.Gender == 'undefined' || request.query.Gender == null
    )
     {
        response.statusCode = 400;
        return response.send(
          {
            status: 'FAILURE',
            message: 'emailId or userName or password or DOB or mobileNo or maritalStatus or city or Gender not provided'
          }
        );
     }
  else                                                                                                                                                                                                                                                     
  {
      User.findOne({emailId: request.query.emailId})
      .exec(function(err, userData)
        {
          //console.log(userData);
          if(err)
          {
            console.log(err);
            response.statusCode = 500;
            return response.send(
              {
                status: 'FAILURE',
                message: 'Something went wrong'
              }
            );
          }
          else if(userData!= null)
          {  
             console.log('UserData',userData)
            response.statusCode = 409;
            return response.send(
              {
                status: 'FAILURE',
                message: 'User already exists.'
              }
            );
          }
          else if(userData == null)
          {
            var user=new User();
            user.emailId=request.query.emailId;
            user.password=request.query.password;
            user.userName=request.query.userName;
            user.DOB=request.query.DOB;
            user.MobileNo=request.query.MobileNo;
            user.Gender=request.query.Gender;
            user.maritalStatus=request.query.maritalStatus;

            user.save(function(err,_result)
             {
              if(err)
              {
                  console.log(err);
                  response.statusCode = 500;
                  return response.send(
                  {
                    status: 'FAILURE',
                    message: 'Internal server error.' 
                  }
                );
              }
              else
              {
                  response.statusCode = 201;
                  return response.send(
                    {
                      status: 'SUCCESS',
                      message: 'user saved sucessfully.',
                    }
                  )
              }
            }
            )
          }
        }
      )
  }
}